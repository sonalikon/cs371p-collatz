#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <assert.h>
using namespace std;

#include <unordered_map>
#include <utility>

// Meta cache and lazy cache
unordered_map<int, int> LAZYCACHE;
int METACACHE[100] = { 
    262, 279, 308, 324, 314, 340, 335, 351, 333, 333, 354, 349, 344, 344, 375, 383,
    370, 347, 365, 360, 373, 386, 368, 443, 368, 363, 407, 407, 389, 371, 371, 384,
    384, 366, 441, 379, 410, 423, 436, 405, 405, 449, 418, 400, 369, 387, 444, 382,
    413, 426, 426, 470, 408, 377, 452, 421, 421, 390, 434, 403, 403, 447, 509, 416,
    416, 429, 442, 385, 398, 442, 504, 411, 411, 424, 393, 424, 468, 437, 406, 468, 
    406, 450, 450, 525, 419, 419, 388, 432, 445, 370, 445, 476, 476, 507, 383, 414,
    414, 458, 427, 396};

// -------------
// collatz_read
// -------------

istream& collatz_read (std::istream& r, int& i, int& j) {
    return r >> i >> j;
}

int calc_cycle(unsigned long long num) {
    assert(0 < num);
    int count = 1;
    // Check if the cycle number is even or odd
    while (1 < num) {
        if (num % 2 != 0) {
			count += 2;
            num += (num >> 1) + 1; 
        } else {
            ++count;
            num = num >> 1;
        }
    }
    assert(0 < count);
    return count;
}

int calc_max (int i, int j) {
    int max_length= -1;
    int num = 1 + (j / 2);
    // Check if you need to do extra calculations
    if (i < num) {
        i = num;
    }
    // Otherwise, use the lazy cache
    for (int k = i; k <= j; ++k) {
		unordered_map<int, int>::iterator iter = LAZYCACHE.find(k);
        int curr = 0;
        if (LAZYCACHE.end() == iter) {
            curr = calc_cycle(k);
            LAZYCACHE.insert(pair<int, int>(k, curr));
        } else {
            curr = iter->second;
        }
        if (max_length < curr) {
            max_length = curr;
        }
    }
    return max_length;
}

// ------------
// collatz_eval
// ------------

int collatz_eval(int i, int j) {
    assert(0 < i);
    assert(0 < j);
	int max_cycle;
    // Switch the two numbers
    if (j < i) {
        int temp = j;
        j = i;
        i = temp;
    }
    assert(i <= j);
    if ((j - i) > (9999)) {
		assert(10000 <= j);
        int current = -1;
        int temp_i = (i - 1) / 10000 + 1;
        // Calculate maximum cycle
        max_cycle = calc_max(i, 10000 * temp_i);

        int temp_j = j / 10000;
        int max_range = min(temp_j, 100);
        current = calc_max(10000 * max_range + 1, j);
        // Check if it's the maximum cycle currently
        if (current > max_cycle) {
            max_cycle = current;
        }
        for (int n = temp_i; n < max_range; ++n) {
            //Check the metacache
            current = METACACHE[n];
            if (current > max_cycle) {
                max_cycle = current;
            }
        }
    } else {
        // If it's small enough, just calculate it
        max_cycle = calc_max(i, j);
    }
    assert(0 < max_cycle);
    return max_cycle;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);}}
